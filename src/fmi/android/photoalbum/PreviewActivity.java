package fmi.android.photoalbum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import fmi.android.photoalbum.adapters.ISwipe;
import fmi.android.photoalbum.adapters.SwipeDetector;
import fmi.android.photoalbum.gallery.Gallery;
import fmi.android.photoalbum.model.Image;

public class PreviewActivity extends Activity implements ISwipe {

    private static final String LOG_TAG = "Preview activity";
    private Button delete;
    private Button edit;
    private Button showOnMap;
    private ImageView imageView;
    private int index;
    private Image image;
    private Gallery gallery;
    private SwipeDetector swipeDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        gallery = Gallery.getInstance();
        index = (int) getIntent().getExtras().getLong("index");
        initImage();
        initButtons();
        swipeDetector = new SwipeDetector(this, this);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return swipeDetector.onTouchEvent(event) ||  super.onTouchEvent(event);
    }

    private void initImage() {
        image = gallery.get(index);
        imageView = (ImageView) findViewById(R.id.image_preview);
        imageView.setImageBitmap(image.getBitmap(this, 500, 500));
    }


    private void initButtons() {
        delete = (Button) findViewById(R.id.btn_delete);
        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete();
            }
        });

        edit = (Button) findViewById(R.id.btn_edit);
        edit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onEdit();
            }
        });

        showOnMap = (Button) findViewById(R.id.btn_show_on_map);
        showOnMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowOnMap();
            }
        });
    }


    protected void onShowOnMap() {
        Log.i(LOG_TAG, "init map index " + index);
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("index", index);
        intent.putExtra("preview", "edit");
        startActivity(intent);
    }

    private void onEdit() {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("index", index);
        startActivity(intent);
    }

    private void onDelete() {
        gallery.deleteImage(image);
        finish();
    }

    @Override
    public void onLeftToRight() {
        index++;
        if (index  == gallery.size()) {
            index = 0;
        }
        initImage();
    }

    @Override
    public void onRightToLeft() {
        index--;
        if (index  == -1) {
            index = gallery.size() - 1;
        }
        initImage();
    }
}
