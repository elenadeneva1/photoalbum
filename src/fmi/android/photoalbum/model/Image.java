package fmi.android.photoalbum.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Address;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

import fmi.android.photoalbum.helper.BitmapHelper;
import fmi.android.photoalbum.helper.GeolocatorHelper;

public class Image {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    private String name;
    private String path;
    private String description;
    private Calendar date;
    private LatLng coordinates;
    private Map<String,Bitmap> bitmaps;

    public Image() {
        this("", "", "", Calendar.getInstance(), new LatLng(0, 0));
        bitmaps = new HashMap<String, Bitmap>();
    }

    public Image(String name,String path, String description, Calendar date, LatLng coordinates) {
        this.name = name;
        this.path = path;
        this.description = description;
        this.date = date;
        this.coordinates = coordinates;
        bitmaps = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getCoordinatesAsString() {
        return String.format(Locale.US, "Latitude:\n  %.4f\nLongtitude:\n  %.4f", coordinates.latitude, coordinates.longitude);
    }

    public LatLng getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(LatLng coordinates) {
        this.coordinates = coordinates;
    }

    public Bitmap getBitmap(Context ctx, int width, int height) {
        String key = width + "" + height;
        Bitmap result = bitmaps.get(key);
        if (result == null) {
            result = BitmapHelper.getScaledBitmap(ctx, Uri.parse(path), width, height);
            bitmaps.put(key, result);
        }
        return result;
    }

    public Image copy() {
        return new Image(name, path, description, (Calendar) date.clone(), new LatLng(coordinates.latitude, coordinates.longitude));
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return name + " " + description + " " + path + " " + getCoordinatesAsString() + getDate().getTime();
    }

    public String getDateAsString() {
        return "Date: " + sdf.format(date.getTime());
    }

    public String getDetails(Context context) {
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(name).append("\n");
        builder.append("Description: ").append(description).append("\n");
        builder.append(getDateAsString()).append("\n");
        Address address = GeolocatorHelper.getAddress(context, coordinates);
        if (address != null) {
            builder.append("Country: ").append(address.getAddressLine(2)).append("\n");
            builder.append("City: ").append(address.getAddressLine(1)).append("\n");
            builder.append("Address: ").append(address.getAddressLine(0)).append("\n");
            if (address.getFeatureName() != null) {
                builder.append("Location : ").append(address.getFeatureName());
            }
        }
        return builder.toString();
    }
}
