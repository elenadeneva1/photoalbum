package fmi.android.photoalbum.model;

public class Coordinates {

    private double x;
    private double y;

    public Coordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Coordinates(Coordinates coordinates) {
        x = coordinates.getX();
        y = coordinates.getY();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Latitude " + y + "Longtitude" + x;
    }
}
