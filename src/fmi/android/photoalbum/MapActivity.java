package fmi.android.photoalbum;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import fmi.android.photoalbum.gallery.Gallery;
import fmi.android.photoalbum.model.Image;

public class MapActivity extends FragmentActivity implements OnMapClickListener {

    private static final String LOG_TAG = "MapActivity";
    private GoogleMap map;
    private Image image;
    private String callerActivity;
    private LatLng coordinates;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initImage();
        initilizeMap();
    }

    private void initImage() {
        Bundle bundle = getIntent().getExtras();
        callerActivity = bundle.getString("activity");
        index = bundle.getInt("index");
        if (index < 0) {
            image = new Image();
            image.setName(bundle.getString("name"));
        } else {
            image = Gallery.getInstance().get(index);
        }
        if (image.getCoordinates() != null) {
            coordinates = new LatLng(image.getCoordinates().latitude, image.getCoordinates().longitude);
        } else {
            coordinates = new LatLng(24, 24);
        }
    }

    private void initilizeMap() {
        if (map == null) {
            map = ((SupportMapFragment)  getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (map == null) {
                Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            } else {
                Log.i(LOG_TAG, "init map index " + index);
                if (index >= 0) {
                    try {
                        Log.i(LOG_TAG, image.getCoordinatesAsString());
                        MapsInitializer.initialize(this);
                        MarkerOptions marker = new MarkerOptions().position(image.getCoordinates()).title(image.getName());
                        marker.icon(BitmapDescriptorFactory.fromBitmap(image.getBitmap(this, 250, 250)));
                        map.addMarker(marker);
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(image.getCoordinates()).zoom(12).build();
                        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    } catch(Exception e) {
                        Log.i("Map", e.getLocalizedMessage());
                    }
                }
                map.setOnMapClickListener(this);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }

    @Override
    public void finish() {
        Intent data = new Intent();
        data.putExtra("lat", coordinates.latitude);
        data.putExtra("lng", coordinates.longitude);
        setResult(RESULT_OK, data);
        super.finish();
    }

    @Override
    public void onMapClick(LatLng coordinates) {
        this.coordinates = coordinates;
        if ("edit".equals(callerActivity)) {
            finish();
        }
    }
}
