package fmi.android.photoalbum;

import java.util.Calendar;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import fmi.android.photoalbum.gallery.Gallery;
import fmi.android.photoalbum.model.Image;

public class EditActivity extends FragmentActivity {

    private static final int GET_COORDINATES = 1;
    private static final String LOG_TAG = "Edit";

    private int index;
    private Image image;
    private Image tempImage;
    private Gallery gallery;
    private Button save;
    private Button editDate;
    private Button editCoordinates;
    private EditText name;
    private EditText description;
    private TextView coordinatesText;
    private TextView dateText;
    private DatePickerFragment dateFragment;
    private OnDateSetListener ondate = new OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            tempImage.getDate().set(Calendar.YEAR, year);
            tempImage.getDate().set(Calendar.MONTH, month);
            tempImage.getDate().set(Calendar.DATE, day);
            dateText.setText(tempImage.getDateAsString());
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        initButtons();
        gallery = Gallery.getInstance();
        Bundle bundle = getIntent().getExtras();
        index = bundle.getInt("index");
        if (index == -1) {
            image = new Image();
            tempImage = image;
            tempImage.setPath(bundle.getString("path"));
        } else {
            image = gallery.get(index);
            tempImage = image.copy();
        }

        dateText = (TextView) findViewById(R.id.date);
        dateText.setText(image.getDateAsString());

        coordinatesText = (TextView) findViewById(R.id.coordinates);
        coordinatesText.setText(image.getCoordinatesAsString());

        name = (EditText) findViewById(R.id.edit_name);
        name.setText(image.getName());

        description = (EditText) findViewById(R.id.edit_description);
        description.setText(image.getDescription());
    }

    private void initButtons() {
        Log.i(LOG_TAG, "Init buttons");
        save = (Button) findViewById(R.id.btn_save);
        save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();
            }
        });

        editDate = (Button) findViewById(R.id.btn_edit_date);
        editDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditDate();
            }
        });

        editCoordinates = (Button) findViewById(R.id.btn_edit_coordinates);
        editCoordinates.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditCoordinates();
            }
        });
    }

    private void onEditCoordinates() {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("index", index);
        intent.putExtra("activity", "edit");
        startActivityForResult(intent, GET_COORDINATES);
        coordinatesText.setText(image.getCoordinatesAsString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GET_COORDINATES) {
            double latitude = data.getDoubleExtra("lat", -1);
            double longtitude = data.getDoubleExtra("lng", -1);
            LatLng coordinates = new LatLng(latitude, longtitude);
            tempImage.setCoordinates(coordinates);
            coordinatesText.setText(tempImage.getCoordinatesAsString());
        }
    }

    protected void onEditDate() {
        dateFragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt("year", tempImage.getDate().get(Calendar.YEAR));
        args.putInt("month", tempImage.getDate().get(Calendar.MONTH));
        args.putInt("day", tempImage.getDate().get(Calendar.DAY_OF_MONTH));
        dateFragment.setArguments(args);
        dateFragment.setCallBack(ondate);
        dateFragment.show(getSupportFragmentManager(), "Date Picker");
    }

    protected void onSave() {
        if (index != -1) {
            gallery.deleteImage(image);
        }
        image.setName(name.getText().toString());
        image.setPath(tempImage.getPath());
        image.setDescription(description.getText().toString());
        image.setDate(tempImage.getDate());
        image.setCoordinates(tempImage.getCoordinates());
        gallery.addImage(image);
        finish();
    }
}
