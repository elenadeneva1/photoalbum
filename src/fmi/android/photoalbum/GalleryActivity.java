package fmi.android.photoalbum;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import fmi.android.photoalbum.gallery.Gallery;
import fmi.android.photoalbum.model.Image;

public class GalleryActivity extends FragmentActivity {

    private static final int GET_PICTURE = 100;
    private static final int EDIT_PICTURE = 200;
    private static final String LOG_TAG = "Gallery";
    private static boolean isListView;

    private ListView listView;
    private GridView gridView;
    private Button changeButton;
    private Button sortButton;
    private Button addButton;
    private Gallery gallery;
    private OnItemLongClickListener longClickListener;
    private OnItemClickListener shortClickListener;
    private ProgressDialog pd;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initListeners();
        if (Gallery.getInstance() == null) {
            load();
        } else {
            init();
        }
    }

    private void init() {
        gallery = Gallery.getInstance();
        if (isListView) {
            setListView();
        } else {
            setGridView();
        }
    }

    private void load() {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                pd = new ProgressDialog(GalleryActivity.this, R.style.dialog);
                pd.setMessage("Loading gallery...\n\nPlease wait.");
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... arg0) {
                Gallery.createInstance(getApplicationContext());
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                if (pd != null) {
                    pd.dismiss();
                }
                init();
            }

        };
        task.execute((Void[])null);
    }

    private void initListeners() {
        shortClickListener = new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), PreviewActivity.class);
                intent.putExtra("index", id);
                startActivity(intent);
            }
        };

        longClickListener = new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View arg1, int position, long id) {
                index = position;
                DialogFragment dialog = new InfoFragment();
                dialog.show(getSupportFragmentManager(), "InfoFragment");
                return true;
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == GET_PICTURE) {
            Uri selectedImage = data.getData();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra("index", -1);
            intent.putExtra("path", selectedImage.toString());
            startActivityForResult(intent, EDIT_PICTURE);
        }
    }

    private void onAddImage() {
        Log.i(LOG_TAG, "Add image");
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GET_PICTURE);
    }

    private void onChangeView() {
        if (isListView) {
            setGridView();
        } else {
            setListView();
        }
    }

    private void setGridView() {
        Log.i(LOG_TAG, gallery.getGridAdapter().getCount() + " grid");
        isListView = false;
        setContentView(R.layout.activity_grid_gallery);

        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setLongClickable(true);
        gridView.setAdapter(gallery.getGridAdapter());
        gridView.setOnItemClickListener(shortClickListener);
        gridView.setOnItemLongClickListener(longClickListener);

        changeButton = (Button) findViewById(R.id.btn_change_view);
        changeButton.setText("List view");
        changeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangeView();
            }
        });

        sortButton = (Button) findViewById(R.id.btn_sort);
        if (gallery.isSortedByName()) {
            sortButton.setText("Sort by Date");
        } else {
            sortButton.setText("Sort by Name");
        }
        sortButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gallery.isSortedByName()) {
                    sortByDate();
                } else {
                    sortByName();
                }
            }
        });

        Log.i(LOG_TAG, "ADD button");
        addButton = (Button) findViewById(R.id.btn_add_image);
        addButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddImage();
            }
        });
    }

    private void setListView() {
        Log.i(LOG_TAG, gallery.getListAdapter().getCount() + " list");
        isListView = true;
        setContentView(R.layout.activity_list_gallery);

        listView = (ListView) findViewById(R.id.listView);
        listView.setLongClickable(true);
        listView.setAdapter(gallery.getListAdapter());
        listView.setOnItemClickListener(shortClickListener);
        listView.setOnItemLongClickListener(longClickListener);

        changeButton = (Button) findViewById(R.id.btn_change_view);
        changeButton.setText("Grid view");
        changeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangeView();
            }
        });

        sortButton = (Button) findViewById(R.id.btn_sort);
        sortButton.setText(gallery.isSortedByName() ? "Sort by Date" : "Sort by Name");
        sortButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gallery.isSortedByName()) {
                    sortByDate();
                } else {
                    sortByName();
                }
            }
        });

        Log.i(LOG_TAG, "ADD button");
        addButton = (Button) findViewById(R.id.btn_add_image);
        addButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddImage();
            }
        });
    }

    private void sortByDate() {
        gallery.sortByDate();
        sortButton.setText("Sort by Name");
        sortButton.refreshDrawableState();
    }

    private void sortByName() {
        gallery.sortByName();
        sortButton.setText("Sort by Date");
        sortButton.refreshDrawableState();
    }

    public Image getImage() {
        return gallery.get(index);
    }
}
