package fmi.android.photoalbum.gallery;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import fmi.android.photoalbum.model.Image;

public class ImageDataSource {
    private static final String LOG_TAG = "ImageDataSource";
    private GallerySQLiteHelper dbHelper;
    private String[] allColumns = {
                    GallerySQLiteHelper.COLUMN_NAME,
                    GallerySQLiteHelper.COLUMN_PATH,
                    GallerySQLiteHelper.COLUMN_DESCRIPTION,
                    GallerySQLiteHelper.COLUMN_DATE,
                    GallerySQLiteHelper.COLUMN_LATITUDE,
                    GallerySQLiteHelper.COLUMN_LONGITUDE,
    };

    public ImageDataSource(Context context) {
        dbHelper = new GallerySQLiteHelper(context);
    }

    public void addImage(Image image) {
        ContentValues values = new ContentValues();
        values.put(GallerySQLiteHelper.COLUMN_NAME, image.getName());
        values.put(GallerySQLiteHelper.COLUMN_DESCRIPTION, image.getDescription());
        values.put(GallerySQLiteHelper.COLUMN_DATE, image.getDate().getTimeInMillis());
        values.put(GallerySQLiteHelper.COLUMN_LATITUDE, image.getCoordinates().latitude);
        values.put(GallerySQLiteHelper.COLUMN_LONGITUDE, image.getCoordinates().longitude);
        values.put(GallerySQLiteHelper.COLUMN_PATH, image.getPath());
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.insert(GallerySQLiteHelper.TABLE_IMAGE, null, values);
        database.close();
    }


    public void deleteImage(Image image) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(GallerySQLiteHelper.TABLE_IMAGE, GallerySQLiteHelper.COLUMN_NAME + "='" + image.getName()+"'", null);
        database.close();
    }

    public List<Image> getAllImages() {
        Log.i(LOG_TAG, "Get images");
        List<Image> images = new ArrayList<Image>();
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(GallerySQLiteHelper.TABLE_IMAGE, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Image image = cursorToImage(cursor);
            images.add(image);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return images;
    }

    private Image cursorToImage(Cursor cursor) {
        Image image = new Image();
        image.setName(cursor.getString(0));
        image.setPath(cursor.getString(1));
        image.setDescription(cursor.getString(2));
        image.getDate().setTimeInMillis(cursor.getInt(3));
        image.setCoordinates(new LatLng(cursor.getDouble(4), cursor.getDouble(5)));
        return image;
    }
}
