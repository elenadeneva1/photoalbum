package fmi.android.photoalbum.gallery;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class GallerySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_IMAGE = "Image";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longtitude";

    private static final String DATABASE_NAME = "gallery.db";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE = "create table "
                    + TABLE_IMAGE + "(" + COLUMN_NAME
                    + " text primary key not null, " + COLUMN_PATH
                    + " text not null, " + COLUMN_DESCRIPTION
                    + " text not null, " + COLUMN_DATE
                    + " integer, " + COLUMN_LATITUDE
                    + " integer, " + COLUMN_LONGITUDE
                    + " integer);";

    public  GallerySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(GallerySQLiteHelper.class.getName(),
                        "Upgrading database from version " + oldVersion + " to "
                                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGE);
        onCreate(db);
    }
}