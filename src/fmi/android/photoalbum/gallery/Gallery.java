package fmi.android.photoalbum.gallery;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import fmi.android.photoalbum.adapters.AlbumGridAdapter;
import fmi.android.photoalbum.adapters.AlbumListAdapter;
import fmi.android.photoalbum.helper.Helper;
import fmi.android.photoalbum.model.Image;

public class Gallery {

    private static Gallery instance;
    private static Helper sortByNameHelper = Helper.getSortByNameHelper();
    private static Helper sortByDateHelper = Helper.getSortByDateHelper();
    private static ImageDataSource imageDAO;

    private List<Image> imagesByName;
    private List<Image> imagesByDate;
    private AlbumListAdapter listAdapter;
    private AlbumGridAdapter gridAdapter;
    private boolean sortedByName = true;

    public static void createInstance(Context context) {
        imageDAO = new ImageDataSource(context);
        List<Image> images = imageDAO.getAllImages();
        instance = new Gallery(images, context);
    }

    public static Gallery getInstance() {
        return instance;
    }

    private Gallery(List<Image> images, Context context) {
        imagesByName = images;
        imagesByDate = new ArrayList<Image>(images);
        sortByDateHelper.sort(imagesByDate);
        sortByNameHelper.sort(imagesByName);
        listAdapter = new AlbumListAdapter(images, context);
        gridAdapter = new AlbumGridAdapter(images, context);
    }

    private void notifyDatasetChanged() {
        listAdapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();
    }

    public void addImage(Image image) {
        imageDAO.addImage(image);
        sortByNameHelper.addImage(image, imagesByName);
        sortByDateHelper.addImage(image, imagesByDate);
        notifyDatasetChanged();
    }

    public void deleteImage(Image image) {
        imagesByName.remove(image);
        imagesByDate.remove(image);
        imageDAO.deleteImage(image);
        notifyDatasetChanged();
    }

    public AlbumListAdapter getListAdapter() {
        return listAdapter;
    }

    public AlbumGridAdapter getGridAdapter() {
        return gridAdapter;
    }

    public boolean isSortedByName() {
        return sortedByName;
    }

    public boolean isSortedByDate() {
        return !sortedByName;
    }

    public  void sortByName() {
        listAdapter.setImages(imagesByName);
        gridAdapter.setImages(imagesByName);
        sortedByName = true;
        notifyDatasetChanged();
    }

    public  void sortByDate() {
        listAdapter.setImages(imagesByDate);
        gridAdapter.setImages(imagesByDate);
        sortedByName = false;
        notifyDatasetChanged();
    }

    public Image get(int index) {
        return sortedByName ? imagesByName.get(index) : imagesByDate.get(index);
    }

    public int size() {
        return imagesByName.size();
    }
}
