package fmi.android.photoalbum.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import fmi.android.photoalbum.R;
import fmi.android.photoalbum.model.Image;

public class AlbumGridAdapter extends AlbumAdapter {

    private static class ViewHolder {
        ImageView imageView;
    }

    public AlbumGridAdapter(List<Image> images, Context  context) {
        super(images, context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewGroup photoView = null;
        ViewHolder viewHolder = null;
        if (convertView == null) {
            photoView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, null);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) photoView.findViewById(R.id.image);
            photoView.setTag(viewHolder);
        } else {
            photoView = (ViewGroup) convertView;
            viewHolder = (ViewHolder) photoView.getTag();
        }
        viewHolder.imageView.setImageBitmap(getItem(position).getBitmap(context, 250, 250));
        return photoView;
    }
}
