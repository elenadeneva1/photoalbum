package fmi.android.photoalbum.adapters;


public interface ISwipe {
    public void onLeftToRight();

    public void onRightToLeft();
}
