package fmi.android.photoalbum.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import fmi.android.photoalbum.R;
import fmi.android.photoalbum.model.Image;

public class AlbumListAdapter extends AlbumAdapter {

    private static class ViewHolder {
        ImageView imageView;
        TextView textView;
    }

    public AlbumListAdapter(List<Image> images, Context context) {
        super(images, context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewGroup photoView = null;
        ViewHolder viewHolder = null;
        if (convertView == null || convertView.getTag() == null) {
            photoView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, null);
            photoView.setTag(viewHolder);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) photoView.findViewById(R.id.imageView);
            viewHolder.textView = (TextView) photoView.findViewById(R.id.textView);
        } else {
            photoView = (ViewGroup) convertView;
            viewHolder = (ViewHolder) photoView.getTag();
        }
        viewHolder.imageView.setImageBitmap(getItem(position).getBitmap(context, 250, 250));
        viewHolder.textView.setText(getItem(position).getName());
        return photoView;
    }
}
