package fmi.android.photoalbum.adapters;

import java.util.List;

import android.content.Context;
import android.widget.BaseAdapter;
import fmi.android.photoalbum.model.Image;

public abstract class AlbumAdapter extends BaseAdapter {

    private List<Image> images;
    protected Context context;

    public AlbumAdapter(List<Image> images, Context context) {
        this.images = images;
        this.context = context;
    }

    public void add(Image image) {
        images.add(image);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    public List<Image> getImages() {
        return images;
    }

    @Override
    public Image getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove(Image image) {
        images.remove(image);
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}