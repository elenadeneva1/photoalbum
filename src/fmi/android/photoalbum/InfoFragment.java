package fmi.android.photoalbum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import fmi.android.photoalbum.model.Image;

public class InfoFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Image image = ((GalleryActivity) getActivity()).getImage();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage("Details\n\n" + image.getDetails(getActivity()));
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Button button =  ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        button.setBackgroundColor(getResources().getColor(R.color.brown));
    }
}
