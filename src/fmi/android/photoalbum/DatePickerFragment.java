package fmi.android.photoalbum;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;

public class DatePickerFragment extends DialogFragment {

    private OnDateSetListener ondateSet;

    public void setCallBack(OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    private int year, month, day;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        return dialog;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Button button =  ((DatePickerDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        button.setBackgroundColor(getResources().getColor(R.color.brown));
    }
}
