package fmi.android.photoalbum.helper;

import java.util.Comparator;
import java.util.List;

import fmi.android.photoalbum.model.Image;

public class Helper {

    public static Helper getSortByNameHelper() {
        return new Helper(new Comparator<Image>() {
            @Override
            public int compare(Image limage, Image rimage) {
                return limage.getName().compareTo(rimage.getName());
            }
        });
    }

    public static Helper getSortByDateHelper() {
        return new Helper(new Comparator<Image>() {
            @Override
            public int compare(Image limage, Image rimage) {
                return limage.getDate().compareTo(rimage.getDate());
            }
        });
    }

    private List<Image> images;
    private Comparator<Image> comparator;

    public Helper(Comparator<Image> comparator) {
        this.comparator = comparator;
    }

    public List<Image> sort(List<Image> images) {
        this.images = images;
        if (images.size() > 1) {
            quickSort(0, images.size() - 1);
        }
        return this.images;

    }

    private void quickSort(int low, int high) {
        int i = low;
        int j = high;
        Image pivot = images.get(low + (high - low) / 2);
        while (i <= j) {
            while (comparator.compare(images.get(i), pivot) < 0) {
                i++;
            }
            while (comparator.compare(images.get(j), pivot) > 0) {
                j--;
            }
            if (i <= j) {
                Image temp = images.get(i);
                images.set(i, images.get(j));
                images.set(j, temp);
                i++;
                j--;
            }
            if (low < j) {
                quickSort(low, j);
            }
            if (i < high) {
                quickSort(i, high);
            }
        }
    }

    public void addImage(Image image, List<Image> images) {
        int low = 0;
        int high = images.size();
        while (low != high) {
            int mid = (low + high) / 2;
            if (comparator.compare(image, images.get(mid)) >= 0) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        images.add(low, image);
    }
}
