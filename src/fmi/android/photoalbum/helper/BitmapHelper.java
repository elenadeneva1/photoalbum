package fmi.android.photoalbum.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

public class BitmapHelper {

    private static final String LOG_TAG = "BitmapHelperException";

    public static Bitmap getScaledBitmap(Context ctx, Uri uri, int targetWidth, int targetHeight) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options outDimens = getBitmapDimensions(ctx, uri);
            int sampleSize = calculateSampleSize(outDimens.outWidth, outDimens.outHeight, targetWidth, targetHeight);
            bitmap = downsampleBitmap(ctx, uri, sampleSize);
        } catch (Exception e) {
            Log.i(LOG_TAG, e.toString());
        }
        return bitmap;
    }

    private static BitmapFactory.Options getBitmapDimensions(Context ctx, Uri uri) throws FileNotFoundException, IOException {
        BitmapFactory.Options outDimens = new BitmapFactory.Options();
        outDimens.inJustDecodeBounds = true;
        InputStream is= ctx.getContentResolver().openInputStream(uri);
        BitmapFactory.decodeStream(is, null, outDimens);
        is.close();
        return outDimens;
    }

    private static int calculateSampleSize(int width, int height, int targetWidth, int targetHeight) {
        float bitmapWidth = width;
        float bitmapHeight = height;

        int bitmapResolution = (int) (bitmapWidth * bitmapHeight);
        int targetResolution = targetWidth * targetHeight;

        int sampleSize = 1;
        if (targetResolution == 0) {
            return sampleSize;
        }
        for (int i = 1; (bitmapResolution / i) > targetResolution; i *= 2) {
            sampleSize = i;
        }

        return sampleSize;
    }

    private static Bitmap downsampleBitmap(Context ctx, Uri uri, int sampleSize) throws FileNotFoundException, IOException {
        Bitmap resizedBitmap;
        BitmapFactory.Options outBitmap = new BitmapFactory.Options();

        outBitmap.inJustDecodeBounds = false;
        outBitmap.inSampleSize = sampleSize;

        InputStream is = ctx.getContentResolver().openInputStream(uri);
        resizedBitmap = BitmapFactory.decodeStream(is, null, outBitmap);
        is.close();

        return resizedBitmap;
    }
}
