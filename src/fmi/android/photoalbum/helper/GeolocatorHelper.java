package fmi.android.photoalbum.helper;

import java.util.List;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

public class GeolocatorHelper {

    public static Address getAddress(Context context, LatLng coordinates) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context);
            if (Math.abs(coordinates.latitude) != 0.000001 || Math.abs(coordinates.longitude) != 0.000001) {
                addresses = geocoder.getFromLocation(coordinates.latitude, coordinates.longitude, 1);
                if (addresses != null && addresses.size() > 0) {
                    return addresses.get(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
